from aiohttp import web
from aiohttp.http_exceptions import HttpBadRequest
from aiohttp.web_exceptions import HTTPMethodNotAllowed
from aiohttp.web import Request, Response
from aiohttp.web_urldispatcher import UrlDispatcher
import inspect
import json




async def getIndex(request):
    try:
        response_obj = {
            'error': 'Access Denied'
        }

        return web.Response(text=json.dumps(response_obj), status=401, content_type='application/json')
    except Exception as e:
        # Bad path where name is not set
        response_obj = {'status': 'failed', 'reason': str(e)}
        # return failed with a status code of 500 i.e. 'Server Error'
        return web.Response(text=json.dumps(response_obj), status=500, content_type='application/json')



